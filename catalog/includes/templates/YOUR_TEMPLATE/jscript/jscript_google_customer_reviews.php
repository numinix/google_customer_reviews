<?php
if(GCR_STATUS == 'true') { 
	if ($_GET['main_page'] == FILENAME_CHECKOUT_SUCCESS) { 
	// get customers last order
	$gcr_order = $db->Execute("SELECT 
		orders_id, 
		customers_email_address, 
		delivery_country,
        billing_country,
		estimated_delivery_date, 
		YEAR(estimated_delivery_date) as year,
		MONTH(estimated_delivery_date) as month,
		DAY(estimated_delivery_date) as day 
		FROM " . TABLE_ORDERS . " 
		WHERE customers_id = " . (int)$_SESSION['customer_id'] . " 
		ORDER BY orders_id DESC 
		LIMIT 1;"
	);
	if ($gcr_order->RecordCount() > 0) {
		// get the country code
        if ($gcr_order->fields['delivery_country'] != '' && $gcr_order->fields['delivery_country'] != null) {
            $countries_name = $gcr_order->fields['delivery_country'];
        } else {
            $countries_name = $gcr_order->fields['billing_country'];
        }
		$countries = $db->Execute("SELECT countries_iso_code_2 FROM " . TABLE_COUNTRIES . " WHERE countries_name = '" . $gcr_order->fields['billing_country'] . "' LIMIT 1;");
		if ($countries->RecordCount() > 0) {
			if ($gcr_order->fields['estimated_delivery_date'] != NULL) {
				$estimated_delivery_date = $gcr_order->fields['year'] . '-' . $gcr_order->fields['month'] . '-' . $gcr_order->fields['day'];
			} else {
				// add the shipping time default to today's date
				$estimated_delivery_date = DATE('Y-m-d', time() + ((int)GCR_ESTIMATE_DELIVERY_TIME * 86400));
			}  	
?> 
<script src="https://apis.google.com/js/platform.js?onload=renderOptIn" async defer></script>

<script>
  window.renderOptIn = function() {
    window.gapi.load('surveyoptin', function() {
      window.gapi.surveyoptin.render(
        {
          "merchant_id": <?php echo GCR_ID; ?>,
          "order_id": "<?php echo $gcr_order->fields['orders_id']; ?>",
          "email": "<?php echo $gcr_order->fields['customers_email_address']; ?>",
          "delivery_country": "<?php echo $countries->fields['countries_iso_code_2']; ?>",
          "estimated_delivery_date": "<?php echo $estimated_delivery_date; ?>"
        });
    });
  }
</script>
<?php }}} ?>
<?php if (GCR_BADGE == 'true') {
    $position_array = array(
      'Bottom Left' => 'BOTTOM_LEFT',
      'Bottom Right' => 'BOTTOM_RIGHT'
    );
?>


<!-- BEGIN GCR Badge Code -->
<script src="https://apis.google.com/js/platform.js?onload=renderBadge"
  async defer>
</script>

<script>
  window.renderBadge = function() {
    var ratingBadgeContainer = document.createElement("div");
      document.body.appendChild(ratingBadgeContainer);
      window.gapi.load('ratingbadge', function() {
        window.gapi.ratingbadge.render(
          ratingBadgeContainer, {
            // REQUIRED
            "merchant_id": <?php echo GCR_ID; ?>,
            // OPTIONAL
            "position": '<?php echo $position_array[GCR_POSITION]; ?>'
          });           
     });
  }
</script>
<!-- END GCR Badge Code -->
<?php }} ?>