<?php

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
    ('Google Customers Reviews Badge Position', 'GCR_POSITION', 'Bottom Right', 'Badge Position (default is bottom left)', " . $configuration_group_id . ", 2, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\"Bottom Left\", \"Bottom Right\"),');");