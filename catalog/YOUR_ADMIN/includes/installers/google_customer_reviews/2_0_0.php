<?php
// uninstall Google Trusted Stores
$db->Execute("DELETE FROM " . TABLE_ADMIN_PAGES . " WHERE page_key = 'configGTS' LIMIT 1;");
$gts_configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'GTS_VERSION' LIMIT 1;");
if ($gts_configuration->RecordCount() > 0) {
	$db->Execute("DELETE FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_id = " . (int)$gts_configuration->fields['configuration_group_id'] . " LIMIT 1;");
	$db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_group_id = " . (int)$gts_configuration->fields['configuration_group_id'] . ";");
}

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
    ('Google Customers Reviews Status', 'GCR_STATUS', 'false', 'Activate Google Customer Reviews?', " . $configuration_group_id . ", 2, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
    ('Google Merchant Center ID', 'GCR_ID', '', 'This is your Google Merchant Center ID listed in your account and in the Google provided javascript code, it will be a numbered ID, for example ID: 123456:', " . $configuration_group_id . ", 3, NOW(), NOW(), NULL, NULL),
    ('Default Shipping Time', 'GCR_ESTIMATE_DELIVERY_TIME', '', 'If your shipping modules do not record the estimated delivery date in the orders table (estimated_delivery_date) enter the number of days for your slowest shipping method here. This value will also be used as a default when the estimated_delivery_date field is blank.', " . $configuration_group_id . ", 3, NOW(), NOW(), NULL, NULL),
    ('Badge', 'GCR_BADGE', 'true', 'Display the optional Google Customer Reviews Bade?', " . $configuration_group_id . ", 9, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),');");
    
global $sniffer;
if (!$sniffer->field_exists(TABLE_ORDERS, 'estimated_delivery_date'))  $db->Execute("ALTER TABLE " . TABLE_ORDERS . " ADD estimated_delivery_date DATE NULL DEFAULT NULL;");